import boto3
import json


def handler(event, context):

    try:
        dynamodb = boto3.resource('dynamodb', 'us-east-1')
        table = dynamodb.Table('ivz-blotter')
        data = table.scan()
        response = \
            {
                'isBase64Encoded': False,
                'statusCode': 200,
                'headers': {},
                'multiValueHeaders': {},
                'body': json.dumps(data['Items'])
            }

        return response

    except Exception as e:
        print(e)
        raise e
