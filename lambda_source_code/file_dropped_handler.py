import csv
import urllib.parse
import boto3
from pathlib import Path

s3 = boto3.client('s3')

dynamodb_table_region = 'us-east-1'

dynamodb = boto3.resource('dynamodb', dynamodb_table_region)


def handler(event, context):

    dynamodb_table = 'ivz-blotter'

    # Get the object from the event

    bucket_name = event['Records'][0]['s3']['bucket']['name']

    csv_file_name = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'], encoding='utf-8')

    output_rows = []

    read_s3_csv(bucket_name, csv_file_name, output_rows)

    status = batch_write(dynamodb_table, output_rows)

    if status:

        print('Data saved')

    else:

        print('Error inserting data')


def batch_write(table_name, rows):

    table = dynamodb.Table(table_name)

    with table.batch_writer() as batch:
        for row in rows:
            batch.put_item(Item=row)

    return True


def read_s3_csv(bucket_name, csv_file_name, output_rows):

    try:

        key_delimiter = "::"

        response = s3.get_object(Bucket=bucket_name, Key=csv_file_name)

        lines = response['Body'].read().decode('utf-8').splitlines()

        rows = csv.DictReader(lines)

        counter = 1

        for row in rows:

            file_name_without_extension = Path(csv_file_name).stem

            short_file_name = file_name_without_extension.replace('TEST_BLOTTER_', '')

            row['FileName'] = csv_file_name

            row['FileNameShortId'] = short_file_name

            security_id = row['FMC ID 1']

            row['SecId'] = security_id

            key = security_id + key_delimiter + csv_file_name + key_delimiter + str(counter)

            row['Key'] = key

            output_rows.append(row)

            counter = counter + 1

        return True

    except Exception as e:

        print(e)

        print('Error reading S3 csv file object {} from bucket {}.'.format(csv_file_name, bucket_name))

        raise e
