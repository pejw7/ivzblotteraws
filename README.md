# IVZ Blotter

*IVZ Blotter Demo System showing use of SAM AND CloudFormation*


## Prerequisites

AWS CLI (ensure a connection to AWS Cloud is configured)
https://aws.amazon.com/cli/

AWS SAM CLI and dependencies
https://aws.amazon.com/serverless/sam/

 
## Deployment

To deploy 

Configure AWS CLI with your credentials

In a terminal window:

SAM BUILD
SAM DEPLOY